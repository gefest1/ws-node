const { Client, RemoteAuth, LegacySessionAuth } = require("whatsapp-web.js");

// Require database
const { MongoStore } = require("wwebjs-mongo");
const mongoose = require("mongoose");
const express = require("express");

let client;

// Load the session data
console.log("START MONGO");
mongoose
  .connect(
    "mongodb+srv://hellomik2002:FS8vgXjqQbbsBmhV@pulse.djaditk.mongodb.net/pulse"
  )
  .then(async () => {
    console.log("FINISHED MONGO");
    const store = new MongoStore({ mongoose: mongoose });
    client = new Client({
      puppeteer: {
        headless: true,
        args: ["--no-sandbox", "--disable-setuid-sandbox"],
      },
      authStrategy: new RemoteAuth({
        store: store,
        backupSyncIntervalMs: 300000,
      }),
    });
    // const client = new Client({ // Creamos una nueva instancia del cliente de WhatsApp authStrategy: new LocalAuth({ // Le decimos que vamos a usar una autenticación local clientId: "client-test" //Un identificador único para el cliente }) })
    client.on("ready", async () => {
      console.log("READY");
      const app = express();
      const port = 3100;
      app.use(express.json());
      app.post("/", (req, res) => {
        const number = req.body["phone"];
        const text = req.body["code"];
        const chatId = number.substring(1) + "@c.us";
        client.sendMessage(chatId, text).catch((e) => console.log(e));
        res.end();
      });
      app.listen(port, () => {
        console.log(`Example app listening on port ${port}`);
      });
      console.log("Client is ready!");
    });

    client.on("message", (message) => {
      if (message.body === "!ping") {
        message.reply("pong");
      }
    });
    client.on("qr", (qr) => {
      console.log("QR RECEIVED", qr);
    });
    client.on("remote_session_saved", () => {
      console.log("SAVED");
    });

    client.initialize();
  });
